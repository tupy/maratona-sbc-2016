# TuPy - Maratona SBC 2016

## Sobre

Este repositório contém as resoluções de todas as fases da Maratona SBC 2016
desenvolvidas pelos membros do TuPy. As resoluções fazem parte do treinamento
dos membros, portanto elas não foram desenvolvidas durante a maratona em si.

Cada fase tem uma pasta própria e cada problema uma subpasta nas mesmas. A
organização dos problemas encontra-se na seguinte forma:

* [Aquecimento Fase 1](aquecimento-fase-1/)
* [Fase 1](fase-1/)
* [Aquecimento Fase Final](aquecimento-fase-final/)
* [Fase Final](fase-final/)
